//---------------------------- Thermal Printer -------------------------//

1 - Install python dependencies:
	
		sudo apt-get install python-serial
		#sudo apt-get install python-imaging-tk

2 - Configure Raspberry Pi permission to work with serial:
	
		sudo usermod -a -G dialout pi

		(watch video: https://youtu.be/TC9V7cotUgw?t=288):
		
			sudo nano /boot/cmdline.txt
				root=/dev/mmcblk0p2 rootfstype=ext4

		Delete the last line here:

			sudo nano /etc/inittab

3 - Install python thermal printer library
	(https://github.com/luopio/py-thermal-printer):

		sudo apt-get install git-core
		cd ~/git
		git clone git://github.com/luopio/py-thermal-printer.git
		sudo nano printer.py:
			At the top:  #!usr/bin/env python

4 - Make sure the file is executable:
		chmod 777 printer.py

5 - Our executable script:
	
		import printer, textwrap

		p = printer.ThermalPrinter(serialport="/dev/ttyAMA0")
		wrapped_text = textwrap.fill("\n Hello lalal .. lllalalla ..a.a.a.a lalalal aaala... \n", 32)
		p.print_text(wrapped_text)
		p.linefeed()
		p.linefeed()
		p.linefeed()