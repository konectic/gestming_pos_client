const DEBUG_MODE = false;
const electron = require('electron');
// Module to control application life.
const {
    app
} = electron;
// Module to create native browser window.
const {
    BrowserWindow
} = electron;

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win;

function createWindow() {
    if (DEBUG_MODE) {
        // Create the browser window.
        win = new BrowserWindow({
            width: 800,
            height: 600
        });
        // Open the DevTools.
        win.webContents.openDevTools({
            mode: 'detach'
        });
    } else {
        // Create the browser window.
        win = new BrowserWindow({
            fullscreen: true
        });
    }

    // and load the index.html of the app.
    win.loadURL(`file://${__dirname}/index.html`);


    // Emitted when the window is closed.
    win.on('closed', () => {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        win = null;
    });
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', () => {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (win === null) {
        createWindow();
    }
});

const DB = require('./database.js')
const VERSIONER = require('./versioner.js')
const {
    ipcMain
} = require('electron');
ipcMain.on('shutdown-system', (event) => {
    require('child_process').exec('npm stop');
    require('child_process').exec('shutdown now');
});


ipcMain.on('print-tiket', (event, tiketData) => {

    var command = "python3 ./printing/gestmint_print.py " +
        tiketData.type + " " +
        "'" + tiketData.store_name + "' " +
        tiketData.total + " " +
        "'" + tiketData.date + "' ";

    if (tiketData.type == 0) {

        /*  tiketData.products.forEach(function(product){
             command += "'" + (product.name ? product.name : "Prod. Generico") + " $" + product.price + " Cant.: " + product.amount + "' ";
         }); */
        command += "'";
        tiketData.products.forEach(function (product, index) {
            command += "" + (product.name ? product.name : "Prod. Generico") + " $" + product.price + " Cant.: " + product.amount + "\n";
        });
        command += "'";
    } else {
        command += "'Epresa: " + tiketData.company + " Nro.: " + tiketData.phone_number + "' ";
    }
    console.log('command: ', command);
    require('child_process').exec(command);
});

//require('child_process').exec('python3 ./printing/gestmint_print.py');