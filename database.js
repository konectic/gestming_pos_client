const { ipcMain } = require('electron');
const Datastore = require('nedb');

// USER DATA BASE ------------------------------------------------------/////
var userDB = new Datastore({
    filename: 'databases/user.db',
    autoload: true
});

ipcMain.on('get-logged-user', (event) => {
    userDB.find({ logged: true }, function(err, users) {
        var user = users[0];
        event.sender.send('get-logged-user-reply', user);
    });
});

ipcMain.on('get-user-byuid', (event, uid) => {
    userDB.find({ uid: uid }, function(err, users) {
        var user = users[0];
        event.sender.send('get-user-byuid-reply', user);
    });
});

ipcMain.on('login-user', (event, user) => { //////////////////
    userDB.update({ uid: user.uid }, { $set: { access_token: user.access_token, logged: true } }, { multi: false }, function(err, numReplaced) {
        var response = { error: err, numRep: numReplaced };
        event.sender.send('login-user-reply', response);
    });
});

ipcMain.on('create-user', (event, newUser) => {
    userDB.insert(newUser, function(err, newUser) {
        if (err) {
            event.sender.send('create-user-reply', {});
        } else {
            event.sender.send('create-user-reply', newUser);
        }
    });
});

ipcMain.on('logout-user', (event, user) => {
    userDB.update({ uid: user.uid }, { $set: { logged: false } }, { multi: false }, function(err, numReplaced) {
        var response = { error: err, numRep: numReplaced };
        event.sender.send('logout-user-reply', response);
    });
});

ipcMain.on('update-user-account', (event, user) => {
    userDB.update({ _id: user._id }, { $set: { account: user.account } }, { multi: false }, function(err, numReplaced) {
        var response = { error: err, numRep: numReplaced };
        event.sender.send('update-user-account-reply', response);
    });
});


// PRODUCTS DATA BASE --------------------------------------------------/////
var productsDB = new Datastore({
    filename: 'databases/products.db',
    autoload: true
});

ipcMain.on('create-product', (event, newProduct) => {
    productsDB.insert(newProduct, function(err, product) {
        if (err) {
            event.sender.send('create-product-reply', {});
        } else {
            event.sender.send('create-product-reply', product);
        }
    });

});

ipcMain.on('update-product', (event, product) => {
    productsDB.update({ _id: product._id }, product, {}, function(err, numReplaced, product) {
        if (err) {
            event.sender.send('update-product-reply', {});
        } else {
            event.sender.send('update-product-reply', product);
        }
    });
});

ipcMain.on('remove-product', (event, productID) => {
    productsDB.remove({ _id: productID }, function(err, numRemoved) {
        if (err) {
            event.sender.send('remove-product-reply', {});
        } else {
            event.sender.send('remove-product-reply', numRemoved);
        }
    })
});

ipcMain.on('get-product', (event, id) => {
    productsDB.findOne({ _id: id }, function(err, product) {
        event.sender.send('get-product-reply', product);
    });
});

ipcMain.on('get-products', (event, args) => {
    productsDB.find({}, function(err, producList) {
        event.sender.send('get-products-reply', producList);
    })
});

ipcMain.on('get-products-by-barcode', (event, searchText) => {
    var expression = searchText;
    var rx = new RegExp(expression);
    productsDB.find({ barcode: rx }, function(err, producList) {
        event.sender.send('get-products-by-barcode-reply', producList);
    });
});

ipcMain.on('get-notuploaded-products', (event, searchText) => {
    productsDB.find({ uploaded: false }, function(err, producList) {
        event.sender.send('get-notuploaded-products-reply', producList);
    });
});

ipcMain.on('get-products-by-name', (event, searchText) => {
    var expression = searchText;
    var rx = new RegExp(expression, 'i');
    productsDB.find({ name: rx }, function(err, producList) {
        event.sender.send('get-products-by-name-reply', producList);
    });
});

ipcMain.on('get-products-last-changed', (event) => {
    productsDB.find({}).sort({ changed: -1 }).limit(1).exec(function(err, productList) {
        var lastChanged = (productList.length > 0 && productList[0]) ? productList[0].changed : null;
        event.sender.send('get-products-last-changed-reply', lastChanged);
    });
});

ipcMain.on('insert-product-list', (event, productList) => {
    productsDB.insert(productList, function(err, newProducts) {
        event.sender.send('insert-product-list-reply', newProducts);
    });
});


// TRANSACTIONS DATA BASE --------------------------------------------------/////
var transactionsDB = new Datastore({
    filename: 'databases/transactions.db',
    autoload: true
});

ipcMain.on('insert-transaction', (event, transaction) => {
    transactionsDB.insert(transaction, function(err, transaction) {
        if (err) {
            event.sender.send('insert-transaction-reply', {});
        } else {
            event.sender.send('insert-transaction-reply', transaction);
        }
    });
});

ipcMain.on('get-transactions', (event, queryData) => {
    transactionsDB.find({ $and: [{ date: { $gte: queryData.dates.from } }, { date: { $lt: queryData.dates.to } }, { user_uid: queryData.uid }] }, function(err, transactionList) {
        if (err) {
            console.error(err);
        }
        event.sender.send('get-transactions-reply', transactionList);
    });
});
// DATA BASE BACKUP --------------------------------------------------/////
ipcMain.on('create-databases-zip', (event, queryData) => {
    var responseError = null;
    var fs = require('fs');
    fs.readFile('databases/transactions.db', 'utf8', function(err, data) {
        if (err) throw err;
        var zip = new require('node-zip')();
        zip.file("transactions.db", data, { base64: false });
        fs.readFile('databases/products.db', 'utf8', function(err, data) {
            zip.file("products.db", data, { base64: false });
            var data = zip.generate({ base64: true, compression: 'DEFLATE' });
            event.sender.send('create-databases-zip-reply', data);
            // ZIP TESTING
            //            var testingBlobData = new Buffer(data, 'base64'); // base64 to blob data.
            //            fs.writeFileSync('databases/backup/test.zip', testingBlobData, 'binary'); // write the zip.
        });
    });

});