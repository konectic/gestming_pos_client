const { ipcMain } = require('electron');
const git = require('simple-git');
var repo = 'master';

ipcMain.on('update-app-version', (event) => {
    git().pull('origin', repo, function(error, update) {
        if (error == null) {
            if (update.summary.changes > 0) {
                require('child_process').exec('npm stop');
                require('child_process').exec('npm restart');
            } else {
                event.sender.send('update-app-version-reply', true);
            }
        }
    });
});