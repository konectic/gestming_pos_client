const {webFrame} = require('electron')
webFrame.setZoomFactor(1.2)

angular.module('app', ['ui.router',
        'ngMaterial',
        'ngMessages',
        'angular-virtual-keyboard',
        'utilitiesModule',
        'apiClientModule',
        'paymentManagerModule',
        'initappModule',
        'userModule',
        'tabsModule',
        'saleModule',
        'phoneChargeModule'
    ])
    .config(['$stateProvider',
        '$urlRouterProvider',
        '$mdThemingProvider',
        'VKI_CONFIG',
        function($stateProvider,
            $urlRouterProvider,
            $mdThemingProvider,
            VKI_CONFIG) {

            // States configurations.         
            $stateProvider
                .state('initapp', {
                    url: '/initapp',
                    templateUrl: 'app/js/modules/initapp_module/templates/welcome.html',
                    controller: 'initappModule.initappController'
                })
                .state('login', {
                    url: '/login',
                    templateUrl: 'app/js/modules/user_module/templates/login.html',
                    controller: 'userModule.loginController'
                })
                .state('tabs', {
                    url: '/tabs',
                    templateUrl: 'app/js/modules/tabs_module/templates/tabs.html',
                    controller: 'tabsModule.tabsController'
                })
                .state('tabs.sale', {
                    url: "/list",
                    templateUrl: 'app/js/modules/sale_module/templates/sale.html',
                    controller: 'saleModule.saleController'
                })
                .state('tabs.products', {
                    url: "/products",
                    templateUrl: 'app/js/modules/sale_module/templates/products.html',
                    controller: 'saleModule.productsController'
                })
                .state('tabs.transactions', {
                    url: "/transactions",
                    templateUrl: 'app/js/modules/sale_module/templates/transactions.html',
                    controller: 'saleModule.transactionsController'
                })
                .state('tabs.account', {
                    url: "/account",
                    templateUrl: 'app/js/modules/user_module/templates/account.html',
                    controller: 'userModule.accountController'
                });
            $urlRouterProvider.otherwise('/initapp');

            // App theming configurations.
            $mdThemingProvider.theme('default')
                .dark()
                .primaryPalette('orange')
                .accentPalette('blue');

            // Virtual Keyboard configuration
            VKI_CONFIG.i18n = {
                '00': "Exibir teclado numérico",
                '01': "Exibir teclado virtual",
                '02': "Selecionar layout do teclado",
                '03': "Teclas mortas",
                '04': "Ligado",
                '05': "Desligado",
                '06': "Fechar teclado",
                '07': "Limpar",
                '08': "Limpar campo",
                '09': "Versão",
                '10': "Diminuir tamanho do teclado",
                '11': "Aumentar tamanho do teclado"
            };
            VKI_CONFIG.layout['Espa\u00f1ol'] = {
                'name': "Spanish",
                'keys': [
                    [
                        ["\u00ba", "\u00aa", "\\"],
                        ["1", "!", "|"],
                        ["2", '"', "@"],
                        ["3", "'", "#"],
                        ["4", "$", "~"],
                        ["5", "%", "\u20ac"],
                        ["6", "&", "\u00ac"],
                        ["7", "/"],
                        ["8", "("],
                        ["9", ")"],
                        ["0", "="],
                        ["'", "?"],
                        ["\u00a1", "\u00bf"],
                        ["Bksp", "Bksp"]
                    ],
                    [
                        ["Tab", "Tab"],
                        ["q", "Q"],
                        ["w", "W"],
                        ["e", "E"],
                        ["r", "R"],
                        ["t", "T"],
                        ["y", "Y"],
                        ["u", "U"],
                        ["i", "I"],
                        ["o", "O"],
                        ["p", "P"],
                        ["`", "^", "["],
                        ["+", "*", "]"],
                        ["\u00e7", "\u00c7", "}"]
                    ],
                    [
                        ["Caps", "Caps"],
                        ["a", "A"],
                        ["s", "S"],
                        ["d", "D"],
                        ["f", "F"],
                        ["g", "G"],
                        ["h", "H"],
                        ["j", "J"],
                        ["k", "K"],
                        ["l", "L"],
                        ["\u00f1", "\u00d1"],
                        ["\u00b4", "\u00a8", "{"],
                        ["Enter", "Enter"]
                    ],
                    [
                        ["Shift", "Shift"],
                        ["<", ">"],
                        ["z", "Z"],
                        ["x", "X"],
                        ["c", "C"],
                        ["v", "V"],
                        ["b", "B"],
                        ["n", "N"],
                        ["m", "M"],
                        [",", ";"],
                        [".", ":"],
                        ["-", "_"],
                        ["Shift", "Shift"]
                    ],
                    [
                        [" ", " ", " ", " "],
                        ["AltGr", "AltGr"]
                    ]
                ],
                'lang': ["es"]
            };
            VKI_CONFIG.layout.numeric = {
                'name': "Numeric",
                'keys': [
                    [
                        ["1", '1'],
                        ["2", "2"],
                        ["3", "3"],
                        ["Bksp", "Bksp"]
                    ],
                    [
                        ["4", "4"],
                        ["5", "5"],
                        ["6", '6'],
                        ["Enter", "Enter"]
                    ],
                    [
                        ["7", "7"],
                        ["8", "8"],
                        ["9", "9"],
                        ["", ""]
                    ],
                    [
                        ["0", "0"],
                        ["", ""],
                        [".", "."],
                        ["", ""]
                    ]
                ],
                'lang': ["pt-BR-num"]
            };
            VKI_CONFIG.layout.numeric_int = {
                'name': "Numeric Integer",
                'keys': [
                    [
                        ["1", '1'],
                        ["2", "2"],
                        ["3", "3"],
                        ["Bksp", "Bksp"]
                    ],
                    [
                        ["4", "4"],
                        ["5", "5"],
                        ["6", '6'],
                        ["Enter", "Enter"]
                    ],
                    [
                        ["7", "7"],
                        ["8", "8"],
                        ["9", "9"],
                        ["", ""]
                    ],
                    [
                        ["0", "0"],
                        ["", ""],
                        ["", ""],
                        ["", ""]
                    ]
                ],
                'lang': ["pt-BR-num"]
            };
        }
    ]);