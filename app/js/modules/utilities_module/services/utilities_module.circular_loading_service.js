angular.module('utilitiesModule').service('utilitiesModule.circularLoadingService', [function(){
    this.showLoading = function(){
        document.getElementById("circularLoading").className = "full-size absolute-centered loading-background";
    }   
    
    this.hideLoading = function(){
        document.getElementById("circularLoading").className = "full-size absolute-centered loading-background hidden";
    }                                                                
}]);