(function() {
    'use strict'

    angular
        .module('initappModule')
        .controller('initappModule.initappController', initappController);

    initappController.$inject = ['$scope', '$state', '$timeout', 'userModule.userService',
        '$mdDialog', 'saleModule.syncService', 'databaseBackupModule.backupService',
        'updaterService'
    ];

    function initappController($scope, $state, $timeout, userService, $mdDialog, syncService, backupService, updaterService) {
        $scope.feedbackMessage = "";

        function showError(message, callback) {
            $mdDialog.show(
                $mdDialog.alert()
                .parent(angular.element(document.body))
                .clickOutsideToClose(true)
                .title('Error')
                .textContent(message)
                .ariaLabel('Expired Session')
                .ok('Ok!')
            ).finally(callback);
        }

        function syncronizeAndGo() {
            $scope.feedbackMessage = "Sincronizando productos...";
            syncService.syncAll().then(function() {
                $state.go("tabs.sale");
            }, function(error) {
                // TODO: manage error, check session error.
                console.error(error);
            });
        }

        function init() {
            $scope.feedbackMessage = "Checkeando Sesión...";
            userService.checkSession().then(function(session) {
                if (session.status) {
                    //                backupService.doBackup().then(function(){
                    syncronizeAndGo()
                        //                }, function(error){
                        // TODO: manage error, check session error.
                        //                    console.error(error);
                        //                });
                } else {
                    if (session.error) {
                        showError(session.error, function() {
                            $state.go("login");
                        });
                    } else {
                        $state.go("login");
                    }
                }
            });
        }

        $scope.$on('$viewContentLoaded', function(event) {
            $scope.feedbackMessage = "Cargando...";
            updaterService.checkUpdate().then(function() {
                init();
            });
        });
    }
})();