angular.module('saleModule').controller('saleModule.checkoutSaleController', ['$scope',
    '$mdDialog',
    '$timeout',
    'paymentManagerModule.paymentManagerService',
    '$rootScope',
    function ($scope,
        $mdDialog,
        $timeout,
        paymentManagerService,
        $rootScope) {
        $scope.card = {};
        $scope.cardNumberFocus = false;
        $scope.secCodFocus = false;

        $timeout(function () {
            $scope.cardNumberFocus = true;
        }, 500);

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        $scope.cardNumberKeypress = function ($event) {
            var keyCode = $event.which || $event.keyCode;
            var ref = this;
            if ($scope.card.number && keyCode === 13) {
                var auxIndex = 0;
                var cardData = $scope.card.number;
                $scope.card.name = cardData
                    .substring((cardData.indexOf("^") + 1), cardData.lastIndexOf("^"))
                    .replace("/", " ")
                    .replace("&", "");
                var expDate = cardData.substring(cardData.lastIndexOf("^") + 1);
                $scope.card.month = expDate.substring(2, 4);
                $scope.card.year = expDate.substring(0, 2);
                $scope.card.number = cardData.substring(2, cardData.indexOf("^"));
                var bin = $scope.card.number.substring(0, 6);
                $timeout(function () {
                    $scope.cardNumberFocus = false;
                    $scope.secCodFocus = true;
                    paymentManagerService.getPayModes(bin, $rootScope.sale.total, function (status, response) {
                        if (status == 200) {
                            $scope.installments = response[0].payer_costs;
                            console.log("installments: ", $scope.installments);
                            if($scope.installments.lenght == 0){
                                $scope.installments = [
                                    { recommended_message: '1 cuotas' },
                                    { recommended_message: '2 cuotas' },
                                    { recommended_message: '3 cuotas' },
                                    { recommended_message: '6 cuotas' },
                                    { recommended_message: '12 cuotas' },
                                ];
                            }
                        }
                    });
                }, 200);
            }
        }

        $scope.onCardNumberFocus = function () {
            paymentManagerService.setUpMercadoPago();
            $scope.cardNumberFocus = true;
            $scope.secCodFocus = false;
            $scope.card = {};
        }

        $scope.ok = function () {
            paymentManagerService.getCardTokenFromMP(function (status, response) {
                if (status == 200) {
                    console.log("MP status: ", status);
                    console.log("MP response: ", response);
                    $scope.card.token = response;
                    paymentManagerService.getPayMethod($scope.card.token.first_six_digits).then(function (payMethod) {
                        $scope.card.payMethod = payMethod;
                        $mdDialog.hide($scope.card);
                    });
                } else {
                    console.error("MP status: ", status);
                    console.error("MP response: ", response);
                    var errorDesc = "";
                    switch (status) {
                        case 500:
                            errorDesc = 'Error: No hay conexión a intertent';
                            break;
                        case 400:
                            if (response.cause[0].code != "011") {
                                var description = "";
                                response.cause.forEach(function (cause) {
                                    switch (cause.code) {
                                        case "316":
                                            description += "El nombre del titular es invalido, ";
                                            break;
                                        case "325":
                                            description += "El mes de expiración es invalido, ";
                                            break;
                                        case "326":
                                            description += "El año de expiración es invalido, ";
                                            break;
                                        case "E301":
                                            description += "El número de la tarjeta es invalido, ";
                                            break;
                                        case "E302":
                                            description += "El código de seguridad es invalido, ";
                                            break;
                                        case "324":
                                            description += "El número de documento es invalido, ";
                                            break;
                                        case "011":
                                            description += "Acción inválida! CODIGO: 001, ";
                                            break;
                                    }
                                });
                                var desc = description ? description : status;
                                errorDesc = "Error: " + description;
                            } else {
                                //                            $scope.card.token = response;
                                //                            paymentManagerService.getPayMethod($scope.card.token.first_six_digits).then(function(payMethod){
                                //                                $scope.card.payMethod = payMethod;
                                //                                $mdDialog.hide($scope.card);
                                //                            });
                            }
                            break;
                    }

                    var alert = $mdDialog.alert()
                        .title('Error al realizar la transacción')
                        .textContent(errorDesc)
                        .ariaLabel('Transaction Error')
                        .ok('Ok');
                    $mdDialog.show(alert).then(function () {
                        $scope.barcodeCatcherText = "";
                        $scope.barcodeCatcherFocus = true;
                    });
                }
            });
        };
    }
]);