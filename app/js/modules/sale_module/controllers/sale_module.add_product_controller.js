angular.module('saleModule').controller('saleModule.addProductController', ['$scope', 
                                                                            '$state', 
                                                                            '$q', 
                                                                            '$timeout', 
                                                                            '$mdDialog', 
                                                                            'saleModule.productsService',
                                                                            'edittingProduct', 
                                                                            'saleModule.syncService', 
                                                                            'userModule.userService', 
                                                                            'utilitiesModule.circularLoadingService', 
                                                                            function($scope,
                                                                                      $state, 
                                                                                      $q, 
                                                                                      $timeout, 
                                                                                      $mdDialog,
                                                                                      productsService,
                                                                                      edittingProduct, 
                                                                                      syncService, 
                                                                                      userService, 
                                                                                      circularLoadingService){
    $scope.editing = edittingProduct ? true : false;
    $scope.addMultipleProducts = true;
    $scope.okMessageActivated = false;                                                                            
    $scope.title = $scope.editing ? "Editar Producto" : "Cargar Producto";  
    $scope.newProduct = $scope.editing ? edittingProduct : {};
                                                                                
    var refreshForm = function(){
        $scope.newProduct = {};
    };                    
              
    $scope.cancel = function(){
        $mdDialog.cancel();
    };                                                                       
                                      
    $scope.processProduct = function(){
        circularLoadingService.showLoading();
        var product = {
            barcode: $scope.newProduct.barcode,
            name: $scope.newProduct.name,
            price: $scope.newProduct.price,
            detail: $scope.newProduct.detail,
            uploaded: $scope.newProduct.uploaded,
            changed: "",
            created: "",
            pid: ""
        }
        syncService.checkProductAtServer(product).then(function(_product){
            if(!$scope.editing){
                syncService.checkProductLocally(_product).then(function(result){
                    if(result.exists){
                        $mdDialog.show(
                            $mdDialog.alert()
                            .parent(angular.element(document.body))
                            .clickOutsideToClose(true)
                            .title('Error')
                            .textContent("Ya existe un producto con codigo de barras: "+_product.barcode)
                            .ariaLabel('Already Exists')
                            .ok('Ok!')
                        );
                    }
                    else{
                        productsService.createProduct(_product, function(){
                            circularLoadingService.hideLoading();
                            if ($scope.addMultipleProducts){
                                refreshForm();
                            }
                            else{
                                $mdDialog.cancel();
                            }
                            $scope.okMessageActivated = true;
                        });
                    }
                });
            }
            else{
                product._id = $scope.newProduct._id;
                productsService.updateProduct(_product, function(){
                    $mdDialog.cancel();
                });
            }
        }, function(error){
            console.error(error);
        });
    };
}]);