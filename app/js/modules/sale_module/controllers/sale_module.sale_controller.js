angular.module('saleModule').controller('saleModule.saleController', ['$scope',
    '$rootScope',
    '$state',
    '$mdDialog',
    'saleModule.productsService',
    '$timeout',
    'saleModule.transactionsService',
    'paymentManagerModule.paymentManagerService',
    'utilitiesModule.circularLoadingService',
    'userModule.userService',
    '$interval',
    '$filter',
    function ($scope,
        $rootScope,
        $state,
        $mdDialog,
        productsService,
        $timeout,
        transactionsService,
        paymentManagerService,
        circularLoadingService,
        userService,
        $interval,
        $filter) {
        const {
            ipcRenderer
        } = require('electron');

        if (!$rootScope.sale)
            $rootScope.sale = {
                total: 0,
                products: []
            }

        $interval(function () {
            if (angular.element(document).find('md-dialog').length == 0) {
                $scope.barcodeCatcherText = "";
                $scope.barcodeCatcherFocus = true;
            } else {
                $scope.barcodeCatcherFocus = false;
            }
        }, 1000);

        $scope.barcodeCatcherFocus = true;

        function setBarcodeCatcherFocus() {
            $timeout(function () {
                if ($scope.barcodeCatcherFocus) {
                    $scope.barcodeCatcherFocus = false;
                    $timeout(function () {
                        $scope.barcodeCatcherFocus = true;
                    });
                }
                setBarcodeCatcherFocus();
            }, 500);
        }

        $scope.calcTotal = function () {
            $rootScope.sale.total = 0;
            for (var i = 0; i < $rootScope.sale.products.length; i++) {
                if (!$rootScope.sale.products[i].deleted) {
                    var currentProd = $rootScope.sale.products[i];
                    $rootScope.sale.total += (currentProd.price * currentProd.amount);
                }
            }
            $rootScope.sale.total = Math.round($rootScope.sale.total  * 100) / 100;
        }

        $scope.onAmountChange = function (product) {
            if (!product.amount || product.amount == 0) {
                $timeout(function () {
                    if (!product.amount || product.amount == 0) {
                        product.amount = 1;
                        $scope.calcTotal();
                    }
                }, 5000);
            } else {
                $scope.calcTotal();
            }
        }

        $scope.removeOne = function (product) {
            product.amount--;
            $scope.calcTotal();
        }
        $scope.addOne = function (product) {
            product.amount++;
            $scope.calcTotal();
        }

        $scope.$on('$viewContentLoaded', function (event) {
            $scope.barcodeCatcherFocus = false;
            $scope.barcodeCatcherFocus = true;
            setBarcodeCatcherFocus();
        });

        $scope.shwoAddProductToCartPopup = function (ev, knownProduct) {
            $scope.barcodeCatcherFocus = false;
            $mdDialog.show({
                controller: function ($scope, $mdDialog, knownProduct) {
                    $scope.product = {
                        _id: knownProduct && knownProduct._id ? knownProduct._id : "",
                        amount: 1,
                        deleted: false,
                        name: knownProduct && knownProduct.name ? knownProduct.name : "",
                        price: knownProduct && knownProduct.price ? knownProduct.price : "",
                        barcode: knownProduct && knownProduct.barcode ? knownProduct.barcode : "",
                        detail: knownProduct && knownProduct.detail ? knownProduct.detail : "",
                    };
                    $scope.cancel = function () {
                        $mdDialog.cancel();
                    };
                    $scope.ok = function (answer) {
                        var prod = {
                            _id: $scope.product._id,
                            price: $scope.product.price,
                            amount: $scope.product.amount,
                            name: $scope.product.name,
                            barcode: $scope.product.barcode,
                            detail: $scope.product.detail
                        }
                        $mdDialog.hide(prod);
                    };
                    $scope.onAmountChange = function () {
                        if (!$scope.product.amount || $scope.product.amount == 0) {
                            $timeout(function () {
                                if (!$scope.product.amount || $scope.product.amount == 0) {
                                    $scope.product.amount = 1;
                                }
                            }, 5000);
                        }
                    }
                    $scope.removeOne = function () {
                        $scope.product.amount--;
                    }
                    $scope.addOne = function () {
                        $scope.product.amount++;
                    }
                },
                templateUrl: 'app/js/modules/sale_module/templates/add_product_to_cart.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: false,
                fullscreen: false,
                locals: {
                    knownProduct: knownProduct
                }
            }).then(function (product) {
                var alreadyProd = $rootScope.sale.products.filter(function (prod) {
                    return prod.barcode && prod.barcode == product.barcode;
                })[0];
                if (alreadyProd) {
                    alreadyProd.amount += parseInt(product.amount);
                } else {
                    $rootScope.sale.products.push(product);
                }
                $scope.calcTotal();
                $scope.barcodeCatcherFocus = true;
            }, function () {
                $scope.barcodeCatcherFocus = true;
            });
        };

        $scope.shwoRemoveProductPopup = function (ev, product) {
            $scope.barcodeCatcherFocus = false;
            var confirm = $mdDialog.confirm()
                .title('Quitar Producto')
                .textContent('Estas seguro que quieres quitar este producto: ' + (product.name ? product.name : "Sin Nombre") + '?')
                .ariaLabel('Remove Product')
                .targetEvent(ev)
                .cancel('Cancelar')
                .ok('Quitar');
            $mdDialog.show(confirm).then(function () {
                product.deleted = true;
                $scope.calcTotal();
                $scope.barcodeCatcherFocus = true;
            }, function () {
                $scope.barcodeCatcherFocus = true;
            });
        };

        $scope.catchBarcode = function ($event) {
            var keyCode = $event.which || $event.keyCode;
            if ($scope.barcodeCatcherText && keyCode === 13) {
                $scope.barcodeCatcherFocus = false;
                productsService.searchProducts($scope.barcodeCatcherText).then(function (matches) {
                    var prod = matches[0];
                    if (prod) {
                        $scope.barcodeCatcherText = "";
                        $scope.shwoAddProductToCartPopup(null, prod);
                    } else {
                        var confirm = $mdDialog.confirm()
                            .title('Producto no encontrado')
                            .textContent('El producto con codigo de barra: ' + $scope.barcodeCatcherText + ' no existe en la base de datos.')
                            .ariaLabel('Not Exists')
                            .ok('Agregar uno genérico')
                            .cancel('Cerrar');
                        $mdDialog.show(confirm).then(function () {
                            $scope.barcodeCatcherText = "";
                            $scope.shwoAddProductToCartPopup();
                        }, function () {
                            $scope.barcodeCatcherText = "";
                            $scope.barcodeCatcherFocus = true;
                        });
                    }
                });
            }
        }

        $scope.checkout = function (ev) {
            $scope.barcodeCatcherText = "";
            $scope.barcodeCatcherFocus = false;
            $mdDialog.show({
                controller: "saleModule.checkoutSaleController",
                templateUrl: 'app/js/modules/sale_module/templates/checkout_sale.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: false,
                fullscreen: false
            }).then(function (card) {
                circularLoadingService.showLoading();
                var d = new Date();
                var transaction = {
                    products: [],
                    type: '0',
                    total: $rootScope.sale.total,
                    date: d.getTime(),
                    user_uid: userService.currentUser.uid,
                    store_name: userService.currentUser.store.name
                };
                $rootScope.sale.products.forEach(function (prod) {
                    if (!prod.deleted) {
                        transaction.products.push({
                            _id: prod._id,
                            price: prod.price,
                            amount: prod.amount,
                            name: prod.name,
                            barcode: prod.barcode,
                            description: prod.description
                        });
                    }
                });

                paymentManagerService.doPay(transaction.total.toString(), transaction, card.token, card.payMethod).then(function (response) {
                    console.log(response);
                    transactionsService.insertTransaction(transaction, function (_transaction) {
                        if (_transaction) {
                            $rootScope.sale = {
                                total: 0,
                                products: []
                            }
                            $scope.barcodeCatcherText = "";
                            $scope.barcodeCatcherFocus = true;
                            transaction.date = $filter('date')(transaction.date, "short");
                            console.log("PRINTING: ", transaction);
                            ipcRenderer.send('print-tiket', transaction);

                            circularLoadingService.hideLoading();
                        } else {
                            circularLoadingService.hideLoading();
                            var alert = $mdDialog.alert()
                                .title('Error')
                                .textContent('Error al realizar la transacción')
                                .ariaLabel('Transaction Error')
                                .ok('Ok');
                            $mdDialog.show(alert).then(function () {
                                $scope.barcodeCatcherText = "";
                                $scope.barcodeCatcherFocus = true;
                            });
                        }
                    });
                }, function (error) {
                    circularLoadingService.hideLoading();
                    console.error(error);
                    if (error.status == 401) {
                        $mdDialog.show(
                            $mdDialog.alert()
                            .parent(angular.element(document.body))
                            .clickOutsideToClose(true)
                            .title('Error')
                            .textContent("Sesión Expirada!")
                            .ariaLabel('Expired Session')
                            .ok('Ok!')
                        ).finally(function () {
                            $state.go("login");
                        });
                    } else {
                        circularLoadingService.hideLoading();
                        var alert = $mdDialog.alert()
                            .title('Error')
                            .textContent('Error al realizar la transacción, Error Code: ' + error.status + ' Error Descrip: ' + (error.data[0] ? error.data[0] : error.data.error_description))
                            .ariaLabel('Transaction Error')
                            .ok('Ok');
                        $mdDialog.show(alert).then(function () {
                            $scope.barcodeCatcherText = "";
                            $scope.barcodeCatcherFocus = true;
                        });
                    }
                });
            });
        }

        $scope.showPhoneChargePopup = function (ev) {
            $scope.barcodeCatcherFocus = false;
            $mdDialog.show({
                controller: "phoneChargeModule.phoneChargeController",
                templateUrl: 'app/js/modules/phone_charge_module/templates/phonecharge.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: false,
                fullscreen: false,
            }).then(function (card) {
                $scope.barcodeCatcherText = "";
                $scope.barcodeCatcherFocus = true;
            });
        }
    }
]);