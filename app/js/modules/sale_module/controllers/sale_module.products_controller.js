angular.module('saleModule').controller('saleModule.productsController', ['$scope', 
                                                                          '$state', 
                                                                          'saleModule.productsService', 
                                                                          '$mdDialog',
                                                                          'utilitiesModule.circularLoadingService',
                                                                          '$timeout',
                                                                          function ($scope, 
                                                                                     $state, 
                                                                                     productsService, 
                                                                                     $mdDialog, 
                                                                                     circularLoadingService, 
                                                                                     $timeout) {
    $scope.shearch = {};

    function refreshProductList(){
        circularLoadingService.showLoading();
        productsService.getProducts(function(productList){
            $scope.products = productList;
            $scope.$apply();
            circularLoadingService.hideLoading();
        });
    }                                                                          
                                                                              
    $scope.$on('$viewContentLoaded', function(event){
        refreshProductList();
    });            
                                                                              
    $scope.shwoAddProductPopup = function(ev, product) {
        $mdDialog.show({
            controller: 'saleModule.addProductController',
            templateUrl: 'app/js/modules/sale_module/templates/add_product.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: false,
            fullscreen: false,
            locals: {edittingProduct: product}
        }).then(null, function(){
            refreshProductList();
        });
    };
                                                                              
    $scope.shwoRemoveProductPopup = function(ev, product) {
        var confirm = $mdDialog.confirm()
        .title('Eliminar Producto')
        .textContent('Estas seguro que quieres eliminar este producto: ' + (product.name ? product.name : "Sin Nombre")+'?')
        .ariaLabel('Remove Product')
        .targetEvent(ev)
        .cancel('Cancelar')
        .ok('Eliminar');
        $mdDialog.show(confirm).then(function() {
            productsService.removeProduct(product._id);
            refreshProductList();
        });
    };
                                                                              
    $scope.startSearch = function(){
        $scope.shearch.showSearch = true;
    }
    $scope.search = function() {
        circularLoadingService.showLoading();
        if($scope.search.searchText.length > 0){
            $timeout(function(){
                productsService.searchProducts($scope.search.searchText).then(function(matches) {
                    circularLoadingService.hideLoading();
                    $scope.products = matches;
                });
            });
        }
    }
    $scope.finishSearch = function(){
        $scope.shearch.showSearch = false;
        refreshProductList();
    }
}]);