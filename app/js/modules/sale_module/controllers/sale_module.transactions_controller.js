angular.module('saleModule').controller('saleModule.transactionsController', ['$scope',
    '$state',
    '$mdDialog',
    'saleModule.transactionsService',
    'userModule.userService',
    function($scope,
        $state,
        $mdDialog,
        transactionsService,
        userService) {
        $scope.filter = {};
        var d = new Date();
        var date = d.getTime();
        var to = date;
        d.setHours(0);
        var from = d.getTime();

        function getTransactionsByDate(from, to) {
            $scope.filter.from = new Date(from);
            $scope.filter.to = new Date(to);
            transactionsService.getTransactions({ from: from, to: to }, userService.currentUser.uid).then(function(_transactions) {
                $scope.transactions = _transactions;
                $scope.total = 0;
                $scope.transactions.forEach(function(transaction) {
                    $scope.total += parseInt(transaction.total);
                });
            });
        }

        $scope.filter = function() {
            var from = $scope.filter.from.getTime();
            $scope.filter.to.setHours(23);
            var to = $scope.filter.to.getTime();
            getTransactionsByDate(from, to);
        }

        $scope.shwoTransactionDetail = function(ev, transaction) {
            $mdDialog.show({
                templateUrl: 'app/js/modules/sale_module/templates/transaction_detail.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: false,
                controller: function($mdDialog, $scope) {
                    $scope.transaction = transaction;
                    $scope.cancel = function() {
                        $mdDialog.cancel();
                    };
                }
            });
        }

        getTransactionsByDate(from, to);
    }
]);