angular.module('saleModule').service('saleModule.productsService', ['$q', '$timeout', function($q, $timeout){

    const {ipcRenderer} = require('electron');
    function isBarCode(x) {
        return x % 1 === 0;
    }
    
    this.createProduct = function(newProduct, callback){
        ipcRenderer.send('create-product', newProduct);
        ipcRenderer.on('create-product-reply', (event, product) => {
            if(callback){
                callback(product);
            }
        });
    }
    
    this.updateProduct = function(product, callback){
        ipcRenderer.send('update-product', product);
        ipcRenderer.on('update-product-reply', (event, _product) => {
            if(callback){
                callback(_product);
            }
        });
    }
    
    this.removeProduct = function(product, callback){
        ipcRenderer.send('remove-product', product);
        ipcRenderer.on('remove-product-reply', (event, product) => {
            if(callback){
                callback();
            }
        });
    }
    
    this.getProductById = function(id, callback){
        ipcRenderer.send('get-product', id);
        ipcRenderer.on('get-product-reply', (event, product) => {
            callback(product);
        });
    }
    
    this.getProducts = function(callback){
        ipcRenderer.send('get-products', "");
        ipcRenderer.on('get-products-reply', (event, productList) => {
            callback(productList);
        });
    }
    
    this.searchProducts = function(searchText) {
        var deferred = $q.defer();
        if(isBarCode(searchText)){
            ipcRenderer.send('get-products-by-barcode', searchText);
            ipcRenderer.on('get-products-by-barcode-reply', (event, productList) => {
                deferred.resolve(productList);
            });
        }
        else{
            ipcRenderer.send('get-products-by-name', searchText);
            ipcRenderer.on('get-products-by-name-reply', (event, productList) => {
                deferred.resolve(productList);
            });
        }
        return deferred.promise;
    };
    
    this.getProductsLastChanged = function(callback){
        ipcRenderer.send('get-products-last-changed', "");
        ipcRenderer.on('get-products-last-changed-reply', (event, lastChanged) => {
            callback(lastChanged);
        });
    }
    
    this.createProducts = function(productList, callback){
        ipcRenderer.send('insert-product-list', productList);
        ipcRenderer.on('insert-product-list-reply', (event, productList) => {
            if(callback){
                callback(productList);
            }
        });
    }
    
    this.getNotUploadedProducts = function(callback){
        ipcRenderer.send('get-notuploaded-products', "");
        ipcRenderer.on('get-notuploaded-products-reply', (event, productList) => {
            callback(productList);
        });
    }
}]);