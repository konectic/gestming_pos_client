angular.module('saleModule').service('saleModule.transactionsService', ['$q', '$timeout', function($q, $timeout) {

    const { ipcRenderer } = require('electron');

    this.insertTransaction = function(transaction, callback) {
        ipcRenderer.send('insert-transaction', transaction);
        ipcRenderer.on('insert-transaction-reply', (event, product) => {
            if (callback) {
                callback(product);
            }
        });
    }

    this.getTransactions = function(dates, user_uid) {
        var deferred = $q.defer();
        var queryData = {
            dates: dates,
            uid: user_uid
        }
        ipcRenderer.send('get-transactions', queryData);
        ipcRenderer.on('get-transactions-reply', (event, transactionList) => {
            deferred.resolve(transactionList);
        });
        return deferred.promise;
    }
}]);