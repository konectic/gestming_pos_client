angular.module('saleModule').service('saleModule.syncService', ['$q', 
                                                                '$timeout', 
                                                                'apiClientModule.apiClient',
                                                                'saleModule.productsService', 
                                                                'userModule.userService', 
                                                                '$mdDialog', 
                                                                'utilitiesModule.circularLoadingService', 
                                                                function( $q, 
                                                                          $timeout, 
                                                                           apiClient, 
                                                                           productsService,
                                                                           userService,
                                                                           $mdDialog,
                                                                           circularLoadingService){
    var ref = this;      
    var newProdctList = [];      
    var index = 0;
                                                                    
    function processServerProducts(productList){
        var deferred = $q.defer();
        var serverProduct = productList[index];
        if(index < productList.length){
            ref.checkProductLocally(serverProduct).then(function(result){
                if(result.exists){
                    if(!result.localProduct.uploaded){
                        result.localProduct.uploaded = true;
                        result.localProduct.changed = serverProduct.changed;
                        result.localProduct.created = serverProduct.created;
                        result.localProduct.pid = serverProduct.pid;
                        productsService.updateProduct(result.localProduct, function(){
                            index++;
                            processServerProducts(productList).then(function(_productList){
                                deferred.resolve(newProdctList);
                            }, function(error){
                                deferred.reject(error);
                            });
                        });
                    }
                    else{
                        index++;
                        processServerProducts(productList).then(function(_productList){
                            deferred.resolve(newProdctList);
                        }, function(error){
                            deferred.reject(error);
                        });
                    }
                }
                else{
                    serverProduct.uploaded = true;
                    serverProduct.price = 0;
                    newProdctList.push(serverProduct);
                    index++;
                    processServerProducts(productList).then(function(_productList){
                        deferred.resolve(newProdctList);
                    }, function(error){
                        deferred.reject(error);
                    });
                }
            });
        }
        else{
            $timeout(function(){
                deferred.resolve(newProdctList);
            }, 0);
        }
        return deferred.promise;
    }               
    
    function performSyncAll(page){
        var deferred = $q.defer();
        productsService.getProductsLastChanged(function(lastChanged){
            console.log("last changed: ", lastChanged);
            apiClient.getProducts(lastChanged, page, userService.currentUser.access_token).then(function(productList){
                if(productList.length > 0){
                    index = 0;
                    processServerProducts(productList).then(function(porcessedProductList){
                        productsService.createProducts(porcessedProductList, function(){
                            deferred.resolve();
                        });
                    }, function(error){
                        deferred.reject(error);
                    });
                }
                else{
                    deferred.resolve();
                }
            }, function(error){
                deferred.reject(error);
            });
        });
        return deferred.promise;
    }     
                                                                    
    this.checkProductLocally = function(product){
        var deferred = $q.defer();
        productsService.searchProducts(product.barcode).then(function(result){
            var checkResult = {};
            if(result.length > 0 && result[0].barcode == product.barcode){
                checkResult.exists = true;
                checkResult.localProduct = result[0];
                deferred.resolve(checkResult);
            }
            else{
                checkResult.exists = false;
                deferred.resolve(checkResult);
            }
        });
        return deferred.promise;
    }                                                                 
                                                                    
    function uploadProducts(products, _index){
        var deferred = $q.defer();
        if(index < products.length){
            var product = products[index];
            ref.uploadProduct(product, userService.currentUser.access_token).then(function(response){
                if(response.status == "ok"){
                    product.uploaded = true;
                }
                product.pid = response.pid;
                product.changed = response.changed;
                product.created = response.created;
                productsService.updateProduct(product, function(){
                    _index++;
                    uploadProducts(products, _index).then(function(){
                        deferred.resolve();
                    }, function(error){
                        deferred.reject(error);
                    });
                });
            }, function(error){
                circularLoadingService.hideLoading();
                console.error(error);
                if(error.status == 401){
                    $mdDialog.show(
                        $mdDialog.alert()
                        .parent(angular.element(document.body))
                        .clickOutsideToClose(true)
                        .title('Error')
                        .textContent("Sesión Expirada!")
                        .ariaLabel('Expired Session')
                        .ok('Ok!')
                    ).finally(function(){
                        deferred.resolve();
                    });
                }
                else{
                    deferred.resolve();
                }
            });
        }
        else{
            deferred.resolve();
        }
        return deferred.promise;
    }                                                                
                                                                    
    this.syncAll = function(){
        var deferred = $q.defer();
        newProdctList = [];
        performSyncAll(0).then(function(){
            productsService.getNotUploadedProducts(function(notUploadedProducts){
                uploadProducts(notUploadedProducts, 0).then(function(){
                    deferred.resolve();
                }, function(error){
                    deferred.reject(error);
                });
            });
        }, function(error){
            deferred.reject(error);
        });
        return deferred.promise;
    };
    
    this.uploadProduct = function(product, access_token) {
        return apiClient.postProduct(product, access_token);
    };
                                                                    
    this.checkProductAtServer = function(product){
        var deferred = $q.defer();
        if(product.uploaded){
            $timeout(function(){
                deferred.resolve(product);
            }, 0);
        }
        else{
            ref.uploadProduct(product, userService.currentUser.access_token).then(function(response){
                console.log(response);
                if(response.status == "ok"){
                    product.uploaded = true;
                }
                product.pid = response.pid;
                product.changed = response.changed;
                product.created = response.created;
                deferred.resolve(product);
            }, function(error){
                circularLoadingService.hideLoading();
                console.error(error);
                if(error.status == 401){
                    $mdDialog.show(
                        $mdDialog.alert()
                        .parent(angular.element(document.body))
                        .clickOutsideToClose(true)
                        .title('Error')
                        .textContent("Sesión Expirada!")
                        .ariaLabel('Expired Session')
                        .ok('Ok!')
                    ).finally(function(){
                        $state.go("login");
                    });
                }
                else{
                    product.uploaded = false;
                    deferred.resolve(product);
                }
            });
        }
        return deferred.promise;
    }          
}]);