angular.module('apiClientModule').service('databaseBackupModule.backupService', ['$http',
    '$q',
    'apiClientModule.apiClient',
    'userModule.userDBService',
    function($http,
        $q,
        apiClient,
        userDBService) {
        const { ipcRenderer } = require('electron');
        var ref = this;

        function createDatabaseZip() {
            var deferred = $q.defer();
            ipcRenderer.on('create-databases-zip-reply', (event, base64Zip) => {
                deferred.resolve(base64Zip);
            });
            ipcRenderer.send('create-databases-zip');
            return deferred.promise;
        }

        ref.doBackup = function() {
            var deferred = $q.defer();
            createDatabaseZip().then(function(base64Zip) {
                // TODO: send backup to server.
            });
            return deferred.promise;
        }

        ref.shouldDoBackup = function() {
            var deferred = $q.defer();
            return deferred.promise;
        }
    }
]);