(function() {
    angular
        .module('updaterModule')
        .service('updaterService', updaterService);

    updaterService.$inject = ['$q', '$interval'];

    function updaterService($q, $interval) {
        const { ipcRenderer } = require('electron');
        var self = this;

        self.checkUpdate = function() {
            var deferred = $q.defer();
            ipcRenderer.on('update-app-version-reply', (event, ok) => {
                deferred.resolve();
            });
            ipcRenderer.send('update-app-version');
            return deferred.promise;
        }
    }
})();