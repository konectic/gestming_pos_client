angular.module('paymentManagerModule').service('paymentManagerModule.paymentManagerService', ['$http',
    '$q',
    'apiClientModule.apiClient',
    'userModule.userDBService',
    function($http,
        $q,
        apiClient,
        userDBService) {

        var self = this;

        function removejscssfile(filename, filetype) {
            var targetelement = (filetype == "js") ? "script" : (filetype == "css") ? "link" : "none" //determine element type to create nodelist from
            var targetattr = (filetype == "js") ? "src" : (filetype == "css") ? "href" : "none" //determine corresponding attribute to test for
            var allsuspects = document.getElementsByTagName(targetelement)
            for (var i = allsuspects.length; i >= 0; i--) { //search backwards within nodelist for matching elements to remove
                if (allsuspects[i] && allsuspects[i].getAttribute(targetattr) != null && allsuspects[i].getAttribute(targetattr).indexOf(filename) != -1)
                    allsuspects[i].parentNode.removeChild(allsuspects[i]) //remove element by calling parentNode.removeChild()
            }
        }

        this.resetMP = function() {
            removejscssfile("https://secure.mlstatic.com/sdk/javascript/v1/mercadopago.js", "js");
            var mp_script = document.createElement('script');
            mp_script.setAttribute('src', 'https://secure.mlstatic.com/sdk/javascript/v1/mercadopago.js');
            document.head.appendChild(mp_script);
        }

        this.setUpMercadoPago = function(submitCallback) {
            Mercadopago.setPublishableKey("TEST-fd6bb7d1-e412-42ef-8021-d2fc63bd35a6");
            Mercadopago.getIdentificationTypes();
            setTimeout(function() {
                function addEvent(el, eventName, handler) {
                    if (el.addEventListener) {
                        el.addEventListener(eventName, handler);
                    } else {
                        el.attachEvent('on' + eventName, function() {
                            handler.call(el);
                        });
                    }
                };

                function getBin() {
                    var ccNumber = document.querySelector('input[data-checkout="cardNumber"]');
                    return ccNumber.value.replace(/[ .-]/g, '').slice(0, 6);
                };

                function guessingPaymentMethod(event) {
                    var bin = getBin();
                    if (event.type == "keyup") {
                        if (bin.length >= 6) {
                            Mercadopago.getPaymentMethod({
                                "bin": bin
                            }, setPaymentMethodInfo);
                        }
                    } else {
                        setTimeout(function() {
                            if (bin.length >= 6) {
                                Mercadopago.getPaymentMethod({
                                    "bin": bin
                                }, setPaymentMethodInfo);
                            }
                        }, 100);
                    }
                };

                function setPaymentMethodInfo(status, response) {
                    if (status == 200) {
                        // do somethings ex: show logo of the payment method
                        var form = document.querySelector('#pay');

                        if (document.querySelector("input[name=paymentMethodId]") == null) {
                            var paymentMethod = document.createElement('input');
                            paymentMethod.setAttribute('name', "paymentMethodId");
                            paymentMethod.setAttribute('type', "hidden");
                            paymentMethod.setAttribute('value', response[0].id);

                            form.appendChild(paymentMethod);
                        } else {
                            document.querySelector("input[name=paymentMethodId]").value = response[0].id;
                        }
                    }
                };

                addEvent(document.querySelector('input[data-checkout="cardNumber"]'), 'keyup', guessingPaymentMethod);
                addEvent(document.querySelector('input[data-checkout="cardNumber"]'), 'change', guessingPaymentMethod);
            }, 3000);
        }

        this.getCardTokenFromMP = function(sdkResponseHandler) {
            var $form = document.querySelector('#pay');
            Mercadopago.createToken($form, sdkResponseHandler);
        }

        this.doPay = function(amount, description, card_token, paymentMethod) {
            var deferred = $q.defer();
            var ref = this;
            userDBService.getLoggedUser(function(user) {
                apiClient.doServerPayment(amount, user.store.store_id, card_token, description, paymentMethod, user.access_token)
                    .then(function(response) {
                        deferred.resolve(response);
                    }, function(error) {
                        deferred.reject(error);
                    })
                    .finally(function() {
                        ref.resetMP();
                    });
            });
            return deferred.promise;
        }

        this.getPayMethod = function(cardBin) {
            var deferred = $q.defer();
            var data = {
                bin: cardBin
            };
            Mercadopago.getPaymentMethod(data, function(status, data) {
                console.log(data);
                if (status == 200) {
                    deferred.resolve(data[0].id);
                } else {
                    console.error("error getting payment method: ", status, data);
                }
            });
            return deferred.promise;
        }
        
        this.getPayModes = function(bin, amount, callback){
            Mercadopago.getInstallments({"bin": bin,"amount": amount}, callback);
        }
    }
]);