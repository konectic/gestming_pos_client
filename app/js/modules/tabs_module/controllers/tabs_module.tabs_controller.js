(function() {
    angular
        .module('tabsModule')
        .controller('tabsModule.tabsController', tabsController);

    tabsController.$inject = ['$scope', '$state', '$interval', '$mdDialog'];

    function tabsController($scope, $state, $interval, $mdDialog) {
        $scope.goToState = function(state) {
            if (state) {
                $state.go(state);
            }
        }

        $interval(function() {
            $scope.dateAndHour = Date.now();
        }, 1000);

        $scope.showSutdownPopup = function(ev) {
            var confirm = $mdDialog.confirm()
                .title('Apagar el Sistema')
                .textContent('Esta seguro que desea apagar el sistema?')
                .ariaLabel('Shutdown')
                .targetEvent(ev)
                .ok('Si')
                .cancel('No');
            $mdDialog.show(confirm).then(function() {
                const { ipcRenderer } = require('electron');
                ipcRenderer.send('shutdown-system');
            });
        }
    }
})();