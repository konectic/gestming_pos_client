angular.module('apiClientModule').service('apiClientModule.apiClient', ['$http', '$q', function ($http, $q) {

    var ref = this;

    // CONSTANTS -----------------------------------------/////////////////////////////////////////////

    var END_POINT = "https://firenzemin.com.ar/prod/api/1.0";
    var USER_ACCESS_CONFIG = {
        uri: "?q=oauth2/token",
        params: {
            client_id: "client_gestmint",
            grant_type: "password",
            client_secret: "dreCks32049Od3rucKessleOcmM",
            response_type: "code",
            scope: "merchant_scope",
        }
    }
    var HEADERS = {
        application_xwwwformurlencoded: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        json: {
            'Content-Type': 'application/json'
        },
        json_bearer: function (acess_token) {
            return {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + acess_token
            }
        }
    };
    // URIs
    var GET_POS_DATA_URI = "/point_of_sale.json";
    var GET_PRODUCTS_URI = "/productos.json";
    var GET_BACKUP_URI = "/backups.json";
    var DO_SERVER_PAYMENT_URI = "/payment_api_new_product.json";
    var POST_PRODUCT_URI = "/productos";
    var POST_BACKUP_URI = "/backups";
    var POST_FILE_URI = "/file";
    var POST_PHONE_TRANSAC_URI = "/phone_credit.json";

    // BASE METHODS ---------------------------------------/////////////////////////////////////////////

    function POST(uri, headers, bodyData) {
        var deferred = $q.defer();
        var url = (END_POINT + uri);

        var request = {
            method: 'POST',
            url: url,
            headers: headers,
            data: bodyData
        };

        $http(request).then(function (response) {
            console.log("POST response: ", response);
            if (response.status === 200) {
                deferred.resolve(response.data);
            } else {
                deferred.reject(response.status);
            }
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    }

    function PUT(uri, headers, bodyData) {
        var deferred = $q.defer();
        var url = (END_POINT + uri);

        var request = {
            method: 'PUT',
            url: url,
            headers: headers,
            data: bodyData
        };

        $http(request).then(function (response) {
            if (response.status === 200) {
                deferred.resolve(response.data);
            } else {
                deferred.reject(response.status);
            }
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    }

    function GET(uri, headers, urlParams) {
        var deferred = $q.defer();
        var url = (END_POINT + uri);
        if (urlParams) {
            url += urlParams;
        }

        var request = {
            method: 'GET',
            url: url,
            headers: headers
        };
        $http(request).then(function (response) {
            if (response.status === 200) {
                deferred.resolve(response.data);
            } else {
                deferred.reject(response.status);
            }
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    }

    // API CLIENT METHODS --------------------------------------/////////////////////////////////////////////

    // Login
    this.serverLogin = function (username, password) {
        var params = USER_ACCESS_CONFIG.params;
        params.username = username;
        params.password = password;
        console.log("API Client - POST Server Login data:", params);
        return POST(USER_ACCESS_CONFIG.uri, HEADERS.json, USER_ACCESS_CONFIG.params);
    };

    //POS (Point of Sale)
    this.getPOSData = function (access_token) {
        console.log("API Client - GET POS_DATA access_token: ", access_token);
        return GET(GET_POS_DATA_URI, HEADERS.json_bearer(access_token));
    }
    this.doServerPayment = function (amount, store_id, card_token, description, paymentMethod, access_token) {
        var data = {
            "amount": amount,
            "store_id": store_id,
            "card": {
                token: card_token.id,
                payment_method: paymentMethod
            },
            "description": JSON.stringify(description)
        }
        console.log("API Client - POST Server Payment:", data);
        return POST(DO_SERVER_PAYMENT_URI, HEADERS.json_bearer(access_token), data);
    }

    // Products
    this.postProduct = function (product, access_token) {
        var data = {
            "name": product.name,
            "barcode": product.barcode.toString(),
            "detail": product.deta
        };
        console.log("API Client - POST Product data:", data);
        return POST(POST_PRODUCT_URI, HEADERS.json_bearer(access_token), data);
    }
    this.getProducts = function (lastChanged, page, access_token) {
        var urlParams = "?page=" + page;
        if (lastChanged) {
            urlParams += "&changed=" + lastChanged;
        }
        console.log("API Client - GET Product params: ", urlParams, "access_token: ", access_token);
        return GET(GET_PRODUCTS_URI, HEADERS.json_bearer(access_token), urlParams);
    }

    // Database Backup
    this.postBackup = function (access_token) {
        var data = {
            "title": "parr",
            "fid": 1
        }
        console.log("API Client - POST Backup data:", data);
        return POST(POST_PRODUCT_URI, HEADERS.json_bearer(access_token), data);
    }
    this.getBackup = function () {

    }

    // Phone transaction.
    this.postPhoneTransac = function (transaction, card_token, card_paymethod, access_token) {
        var data = {
            amount: transaction.total,
            store_id: transaction.store_id,
            card: {
                token: card_token.id,
                payment_method: card_paymethod
            },
            phone: {
                area_code: transaction.phone_number.substring(0, 3),
                number: transaction.phone_number.substring(3),
                company: transaction.company
            },
            description: JSON.stringify(transaction)
        }
        console.log("API Client - POST Phone transac: ", data);
        return POST(POST_PHONE_TRANSAC_URI, HEADERS.json_bearer(access_token), data);
    }
}]);