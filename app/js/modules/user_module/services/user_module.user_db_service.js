angular.module('userModule').service('userModule.userDBService', [function(){
    
    const {ipcRenderer} = require('electron');
        
    this.saveUser = function(user, callback){
        ipcRenderer.send('create-user', user);
        ipcRenderer.on('create-user-reply', (event, user) => {
            if(callback){
                callback();
            }
        });
    }     
 
    this.getLoggedUser = function(callback){
        ipcRenderer.send('get-logged-user', "");
        ipcRenderer.on('get-logged-user-reply', (event, user) => {
            if(callback){
                callback(user);
            }
        });
    }   
    
    this.getUserByUID = function(uid, callback){
        ipcRenderer.send('get-user-byuid', uid);
        ipcRenderer.on('get-user-byuid-reply', (event, _user) => {
            if(callback){
                callback(_user);
            }
        });
    }          
    
    this.loginUser = function(user, callback){ /////////
        ipcRenderer.send('login-user', user);
        ipcRenderer.on('login-user-reply', (event, user) => {
            if(callback){
                callback();
            }
        });
    }    
    
    this.logoutUser = function(user, callback){
        ipcRenderer.send('logout-user', "");
        ipcRenderer.on('logout-user-reply', (event) => {
            if(callback){
                callback();
            }
        });
    }
    
    this.updateUserAccount = function(user, callback){
        ipcRenderer.send('update-user-account', user);
        ipcRenderer.on('update-user-account-reply', (event, user) => {
            if(callback){
                callback();
            }
        });
    }
}]);