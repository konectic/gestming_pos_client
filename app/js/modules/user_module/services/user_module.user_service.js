angular.module('userModule').service('userModule.userService', ['$q',
                                                                'apiClientModule.apiClient',
                                                                'userModule.userDBService',
                                                                function($q,
                                                                          apiClient,
                                                                          userDBService){
    var ref = this;
    ref.currentUser = {};                                                                
    ref.login = function(usr, pass){
        var deferred = $q.defer();
        userDBService.getLoggedUser(function(oldUser){
            apiClient.serverLogin(usr, pass).then(function(serverUserData){
                apiClient.getPOSData(serverUserData.access_token).then(function(posData){
                    // merge data
                    serverUserData.store = posData.store;
                    serverUserData.account = posData.account;
                    serverUserData.uid = posData.user.uid;
                    serverUserData.logged = true;
                    
                    if(!oldUser){ // there is no logged user
                        userDBService.getUserByUID(serverUserData.uid, function(foundUser){ // check if server user exists
                            if(foundUser){ // already exists
                                serverUserData._id = foundUser._id;
                                userDBService.loginUser(serverUserData, function(){ // update user
                                    ref.currentUser = serverUserData;
                                    deferred.resolve(serverUserData);
                                }, function(error){
                                    deferred.reject(error);
                                });
                            }
                            else{ // doesn't exists
                                userDBService.saveUser(serverUserData, function(){ // register user
                                    ref.currentUser = serverUserData;
                                    deferred.resolve(serverUserData);
                                }, function(error){
                                    deferred.reject(error);
                                });
                            }
                        });
                    }
                    else{ // there is logged user
                        serverUserData._id = oldUser._id;
                        if(serverUserData.uid == oldUser.uid){ // It's the same user
                            userDBService.loginUser(serverUserData, function(){ // update user
                                ref.currentUser = serverUserData;
                                deferred.resolve(serverUserData);
                            }, function(error){
                                deferred.reject(error);
                            });
                        }
                        else{ // It is not the same user
                            userDBService.logoutUser(oldUser, function(){ // logout olduser
                                userDBService.getUserByUID(function(foundUser){ // check if server user exists
                                    if(foundUser){ // already exists
                                        serverUserData._id = foundUser._id;
                                        userDBService.loginUser(serverUserData, function(){ // update user
                                            ref.currentUser = serverUserData;
                                            deferred.resolve(serverUserData);
                                        }, function(error){
                                            deferred.reject(error);
                                        });
                                    }
                                    else{ // doesn't exists
                                        userDBService.saveUser(serverUserData, function(){ // register user
                                            deferred.resolve(serverUserData);
                                        }, function(error){
                                            deferred.reject(error);
                                        });
                                    }
                                });
                            }, function(error){
                                deferred.reject(error);
                            });
                        }
                    }
                });
            }, function(error){
                deferred.reject(error);
            });
        });
        return deferred.promise;
    }
    
    ref.checkSession = function(){
        var deferred = $q.defer();
        var session = {};
        userDBService.getLoggedUser(function(user){
            if(user){
                ref.currentUser = user;
                apiClient.getPOSData(user.access_token).then(function(posData){
                    session.status = true;
                    session.error = null;
                    deferred.resolve(session);
                }, function(error){
                    console.error("Checking session error:", error);
                    if(error.status == 401){
                        session.status = false;
                        session.error = "Sesión expirada!";
                        deferred.resolve(session);
                    }
                    else if(error.status == -1){
                        session.status = false;
                        session.error = "No se pudo establecer conexión con el servidor.";
                        deferred.resolve(session);
                    }
                    else{
                        deferred.reject(error);
                    }
                });
            }
            else{
                session.status = false;
                session.error = null;
                deferred.resolve(session);
            }
        });
        return deferred.promise;
    }
    
    ref.getUser = function(){
        var deferred = $q.defer();
        userDBService.getLoggedUser(function(user){
            ref.currentUser = user;
            apiClient.getPOSData(user.access_token).then(function(posData){
                user.account = posData.account;
                user._id = user._id;
                userDBService.updateUserAccount(user, function(){
                    deferred.resolve(user);
                });
            }, function(error){
                console.error("Checking session error:", error);
                if(error.status == 401){
                    var _error = {
                        status: 401,
                        error: "Sesión expirada!"
                    };
                    deferred.reject(_error);
                }
                else{
                    deferred.reject(error);
                }
            });
        });
        return deferred.promise;
    }
    
    ref.logout = function(){
        var deferred = $q.defer();
        userDBService.logoutUser(ref.currentUser, function(){ // update user
            deferred.resolve();
        }, function(error){
            deferred.reject(error);
        });
        return deferred.promise;
    };
}]);