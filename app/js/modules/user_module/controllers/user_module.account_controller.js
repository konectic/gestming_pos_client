angular.module('userModule').controller('userModule.accountController', ['$scope',
    '$state',
    'userModule.userService',
    '$mdDialog',
    '$timeout',
    'utilitiesModule.circularLoadingService',
    function($scope,
        $state,
        userService,
        $mdDialog,
        $timeout,
        circularLoadingService) {
        $scope.user;
        circularLoadingService.showLoading();
        userService.getUser().then(function(user) {
            $scope.user = user;
            circularLoadingService.hideLoading();
        }, function(error) {
            circularLoadingService.hideLoading();
            $mdDialog.show(
                $mdDialog.alert()
                .parent(angular.element(document.body))
                .clickOutsideToClose(true)
                .title('Error')
                .textContent((error.data ? error.data.error_description : error.error))
                .ariaLabel('Login Error Alert')
                .ok('Ok!')
            );
        });

        $scope.logout = function(ev) {
            var confirm = $mdDialog.confirm()
                .title('Cerrar Sesión')
                .textContent('Esta seguro que desea cerrar sesión?')
                .ariaLabel('Logout')
                .targetEvent(ev)
                .ok('Si')
                .cancel('No');
            $mdDialog.show(confirm).then(function() {
                userService.logout().then(function() {
                    $state.go("login");
                }, function(error) {
                    $mdDialog.show(
                        $mdDialog.alert()
                        .parent(angular.element(document.body))
                        .clickOutsideToClose(true)
                        .title('Error')
                        .textContent((error.data ? error.data.error_description : error.error))
                        .ariaLabel('Login Error Alert')
                        .ok('Ok!')
                    );
                });
            });
        };
    }
]);