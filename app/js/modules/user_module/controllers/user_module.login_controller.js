angular.module('userModule').controller('userModule.loginController', ['$scope', 
                                                                       '$state', 
                                                                       'userModule.userService',
                                                                       '$mdDialog',
                                                                       'saleModule.syncService',
                                                                       function ($scope, 
                                                                                  $state, 
                                                                                  userService, 
                                                                                  $mdDialog,
                                                                                  syncService) {
    $scope.loading = false;
    $scope.enterButton = function(ev){
        $scope.loading = true;
        userService.login($scope.user.name, $scope.user.password).then(function(user){
            $scope.loading = false;
            syncService.syncAll().then(function(){
                $state.go("tabs.sale");
            }, function(error){
                console.error(error);
            });
        }, function(error){
            $scope.loading = false;
        	console.error("Login error:", error);
            $mdDialog.show(
                $mdDialog.alert()
                .parent(angular.element(document.body))
                .clickOutsideToClose(true)
                .title('Login Error')
                .textContent('Error: ' + error.data.error_description)
                .ariaLabel('Login Error Alert')
                .ok('Ok!')
                .targetEvent(ev)
            );
        });
    }                                                                             
}]);