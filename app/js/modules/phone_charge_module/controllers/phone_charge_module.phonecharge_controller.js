(function () {
    angular
        .module('phoneChargeModule')
        .controller('phoneChargeModule.phoneChargeController', phoneChargeController);

    phoneChargeController.$inject = ['$scope', '$mdDialog', 'utilitiesModule.circularLoadingService',
        'userModule.userService', 'paymentManagerModule.paymentManagerService', 'saleModule.transactionsService',
        'apiClientModule.apiClient', 'userModule.userService', '$filter'
    ];

    function phoneChargeController($scope, $mdDialog, circularLoadingService, userService,
        paymentManagerService, transactionsService, apiClient, userService, $filter) {
        const {
            ipcRenderer
        } = require('electron');
        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        $scope.showCheckoutPopup = function () {
            $mdDialog.show({
                controller: "saleModule.checkoutSaleController",
                templateUrl: 'app/js/modules/sale_module/templates/checkout_sale.html',
                parent: angular.element(document.body),
                clickOutsideToClose: false,
                fullscreen: false
            }).then(function (card) {
                circularLoadingService.showLoading();
                var d = new Date();
                var _company = "";
                console.log($scope.phone.company);
                switch ($scope.phone.company) {
                    case "0":
                        _company = "claro";
                        break;
                    case "1":
                        _company = "movistar";
                        break;
                    case "2":
                        _company = "personal";
                        break;
                }
                var transaction = {
                    type: '1',
                    total: $scope.phone.amount,
                    date: d.getTime(),
                    user_uid: userService.currentUser.uid,
                    store_id: userService.currentUser.store.store_id,
                    phone_number: $scope.phone.phoneNumber,
                    company: _company,
                    store_name: userService.currentUser.store.name
                };
                console.log('transaction: ', transaction);
                apiClient.postPhoneTransac(transaction, card.token, card.payMethod, userService.currentUser.access_token).then(function (response) {
                    console.log();
                    if (response.status != "ERROR") {
                        transactionsService.insertTransaction(transaction, function (_transaction) {
                            if (_transaction) { // OK
                                transaction.date = $filter('date')(transaction.date, "short");
                                console.log("PRINTING: ", transaction);
                                ipcRenderer.send('print-tiket', transaction);
                                circularLoadingService.hideLoading();
                            } else { // ERROR
                                circularLoadingService.hideLoading();
                                var alert = $mdDialog.alert()
                                    .title('Error')
                                    .textContent('Error al realizar la transacción')
                                    .ariaLabel('Transaction Error')
                                    .ok('Ok');
                                $mdDialog.show(alert);
                            }
                        });
                    } else {
                        if (response.code == 400) {
                            circularLoadingService.hideLoading();
                            $mdDialog.show(
                                $mdDialog.alert()
                                .parent(angular.element(document.body))
                                .clickOutsideToClose(true)
                                .title('Error')
                                .textContent("Número te teléfono inválido!")
                                .ariaLabel('invalid phone number')
                                .ok('Ok!'));
                        }
                    }
                }, function (error) {
                    circularLoadingService.hideLoading();
                    console.error(error);
                    if (error.status == 401) {
                        $mdDialog.show(
                            $mdDialog.alert()
                            .parent(angular.element(document.body))
                            .clickOutsideToClose(true)
                            .title('Error')
                            .textContent("Sesión Expirada!")
                            .ariaLabel('Expired Session')
                            .ok('Ok!')
                        ).finally(function () {
                            $state.go("login");
                        });
                    } else {
                        circularLoadingService.hideLoading();
                        var alert = $mdDialog.alert()
                            .title('Error')
                            .textContent('Error al realizar la transacción, Error Code: ' + error.status + ' Error Descrip: ' + (error.data[0] ? error.data[0] : error.data.error_description))
                            .ariaLabel('Transaction Error')
                            .ok('Ok');
                        $mdDialog.show(alert);
                    }
                }).finally(function () {
                    paymentManagerService.resetMP();
                });
            });
        };
    }
})();